#pragma once

#include "io.h"

const uint min_img_size = 50; // minimal side of the image shouldn't be less than this

const int M_MSE = 1;
const int M_CC = 2;
const int max_shift_size = 16;

double get_metric(Image &i1, Image &i2, int h_shift, int w_shift, int metric);
int get_shift(Image i1, Image i2, int &h_shift, int &w_shift, int &min);

class Converter{
public:
	uint h, w;
	Image R, G, B; // Image class is used because of code written previously
				   // and to make intermediate results easily printed
	// uint multiplier;
	Converter(Image &source);
	~Converter(){}
	Image get_R(){return R;}
	Image get_G(){return G;}
	Image get_B(){return B;}
	void align(int max_shift);
	// void set_size(uint nh, uint nw){h = nh; w = nw;}
	// void set_R(Image &src){R = src;}
	// void set_G(Image &src){G = src;}
	// void set_B(Image &src){B = src;}
	Image get_image(); // this will cook image from 3 channels
};