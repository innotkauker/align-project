#pragma once

#include "io.h"
#include "matrix.h"
#include "functions.h"

using std::tuple;

class Canny{
	Image source, result;
	uint h, w, t1, t2;
	Matrix<double> G; // gradient abs matrix
	Matrix<bool> visited; // visited pixels
	// Matrix<int> edges;
	bool is_edge(uint x, uint y); // auxillary function to be called during edge detection
public:
	Canny(const Image &src, uint th1, uint th2);
	~Canny(){};
	Image &get_result(){return result;}
};