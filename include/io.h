#pragma once

#include "matrix.h"
#include "EasyBMP.h"

#include <tuple>

// const double M_PI = 3.14;

typedef Matrix<std::tuple<uint, uint, uint>> Image;
typedef Matrix<std::tuple<double, double, double>> Sobel;
typedef std::tuple<uint, uint, uint> Pixel;
typedef std::tuple<double, double, double> extPixel;

Image load_image(const char*);
void save_image(const Image&, const char*);