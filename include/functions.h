#pragma once

#include "io.h"

typedef Matrix<uint> Borders; // border size <top, right, bottom, left>

Pixel operator*(Pixel x, double y); 
Pixel operator+(Pixel x, Pixel y);
Pixel normalize(Pixel x);
Pixel sqrt(Pixel x);
Image gray_world(Image &source);
Image custom(const Image &source, Matrix<double> kernel, bool normalize = true);
Sobel customS(const Image &source, Matrix<double> kernel);
Image sobel_x(const Image &source, bool norm = false);
Image sobel_y(const Image &source, bool norm = false);
Sobel Ssobel_x(const Image &source);
Sobel Ssobel_y(const Image &source);
Image gaussian(const Image &source, double sigma, double radius);
Image gaussian_separable(const Image &source, double sigma, double radius);
Borders get_borders(const Image &src);
uint get_top_b_size(const Image &source);
void replaceStrChar(std::string &str, char replace, char ch);
inline int max(int a, int b){
    return a > b ? a : b;
}
inline int min(int a, int b){
    return a > b ? b : a;
}
Image resize(const Image &source, double scale);
Image autocontrast(const Image &source, double fraction);
Pixel get_pixel(const Image *source, int row, int col);
double get_double(const Matrix<double> *source, int row, int col);