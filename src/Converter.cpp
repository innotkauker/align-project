#include <iostream>
#include <algorithm>
#include "Converter.h"
#include "functions.h"

using std::tuple;
using std::get;
using std::make_tuple;
using std::cout;
using std::cerr;
using std::endl;

Converter::Converter(Image &src): h(src.n_rows / 3), w(src.n_cols), R(h, w), G(h, w), B(h, w)/*, multiplier(1)*/{
    for(uint i = 0; i < h; i++)
        for(uint j = 0; j < w; j++){
            R(i, j) = make_tuple(get<0>(src(2*h + i, j)),
                                        get<0>(src(2*h + i, j)),
                                        get<0>(src(2*h + i, j)));
            G(i, j) = make_tuple(get<0>(src(h + i, j)),
                                        get<0>(src(h + i, j)),
                                        get<0>(src(h + i, j)));
            B(i, j) = make_tuple(get<0>(src(i, j)),
                                        get<0>(src(i, j)),
                                        get<0>(src(i, j)));
        }
}

Image Converter::get_image(){
	Image result(h, w);
    // cout << h << ' ' << w << endl;
	for(uint i = 0; i < h; i++)
        for(uint j = 0; j < w; j++)
        	result(i, j) = make_tuple(get<0>( R(i, j) ), get<0>( G(i, j) ), get<0>( B(i, j) ) );
    return result;
}

int get_shift(Image i1, Image i2, int &h_shift, int &w_shift, int &minimum){
    uint h1 = i1.n_rows, w1 = i1.n_cols, h2 = i2.n_rows, w2 = i2.n_cols;
    int init_h_shift = h_shift / 2, init_w_shift = w_shift / 2;
    uint min = -1;
    int mini = 0, minj = 0;
    int lower_min;
    // cout << "level++" << endl;
    if(!(h1 > min_img_size && h2 > min_img_size && w1 > min_img_size && w2 > min_img_size)){
        return 1; // we should check all shifts for images of higher resolution
    }
    if(h_shift > 1 && w_shift > 1 && // we can decrease resolution
        !get_shift(resize(i1, 0.5), resize(i2, 0.5), init_h_shift, init_w_shift, lower_min)){
            // not the lowest level, check only 4 shifts 
            for(int i = 2*init_h_shift - 1; i <= 2*init_h_shift + 1; i++)
                for(int j = 2*init_w_shift - 1; j <= 2*init_w_shift + 1; j++){
                    double t = get_metric(i1, i2, i, j, M_MSE);
                    cout << "get_metric on " << i << ", " << j << " returned " << t << endl;
                    if(t < min){
                        min = t;
                        mini = i;
                        minj = j;
                    }
                }
            h_shift = mini;
            w_shift = minj;
            minimum = min;
            cout << "h_shift = " << h_shift << ", w_shift = " << w_shift << endl;
    } else {
        // this is the lowest recursion level
        // cout << "bottom ov stack here, h_shift = " << h_shift << ", w_shift = " << w_shift << endl;
        for(int i = -h_shift; i <= h_shift; i++)
            for(int j = -w_shift; j <= w_shift; j++){
                double t = get_metric(i1, i2, i, j, M_MSE);
                cout << "get_metric on " << i << ", " << j << " returned " << t << endl;
                if(t < min){
                    min = t;
                    mini = i;
                    minj = j;
                }
            }
        h_shift = mini;
        w_shift = minj;
        minimum = min;
        cout << "h_shift = " << h_shift << ", w_shift = " << w_shift << endl;
    }
    return 0;
}

void Converter::align(int max_shift){
    int RGh_shift = max_shift, RGw_shift = max_shift, minimum;
    get_shift(R, G, RGh_shift, RGw_shift, minimum);
    cout << "results: " << RGh_shift << ' ' << RGw_shift << ' ' << max_shift << endl;
    // if (abs(RGh_shift) > max_shift || abs(RGh_shift) > max_shift){
        cout << "shift decreased" << endl;
        int nRGh_shift = /*(abs(RGh_shift) > max_shift ? */max_shift/4/* : max_shift)*/;
        int nRGw_shift = /*(abs(RGw_shift) > max_shift ? */max_shift/4/* : max_shift)*/;
        int nmin;
        get_shift(R, G, nRGh_shift, nRGw_shift, nmin);
        cout << "results: " << nmin << ' ' << minimum << endl;
        if(!(nmin > minimum + 100)){ // success
            cout << "success" << endl;
            RGh_shift = nRGh_shift;
            RGw_shift = nRGw_shift;
        }
    // }
    int RBh_shift = max_shift, RBw_shift = max_shift;
    get_shift(R, B, RBh_shift, RBw_shift, minimum);
    cout << "results: " << RBh_shift << ' ' << RBw_shift << ' ' << max_shift << endl;
    // if (abs(RBh_shift) > max_shift || abs(RBw_shift) > max_shift){
        cout << "shift decreased" << endl;
        int nRBh_shift = /*(abs(RBh_shift) > max_shift ? */max_shift/4/* : max_shift)*/;
        int nRBw_shift = /*(abs(RBw_shift) > max_shift ? */max_shift/4/* : max_shift)*/;
        // int nmin;
        get_shift(R, B, nRBh_shift, nRBw_shift, nmin);
        cout << "results: " << nmin << ' ' << minimum << endl;
        if(!(nmin > minimum + 100)){ // wow, success
            cout << "shift decreased" << endl;
            RBh_shift = nRBh_shift;
            RBw_shift = nRBw_shift;
        }
    // }
    // cout << "align results: " << RGh_shift << ' ' << RGw_shift << ' ' << RBh_shift << ' ' << RBw_shift << ' ' << endl;
    // apply shifting, cutting left edges
    int int_t = max(0, RGh_shift),
        int_l = max(0, RGw_shift), 
        int_b = min(R.n_rows, G.n_rows + RGh_shift), 
        int_r = min(R.n_cols, G.n_cols + RGw_shift);
    int int2_t = max(0, RBh_shift),
        int2_l = max(0, RBw_shift), 
        int2_b = min(R.n_rows, B.n_rows + RBh_shift), 
        int2_r = min(R.n_cols, B.n_cols + RBw_shift);
    int_t = max(int_t, int2_t),
    int_l = max(int_l, int2_l), 
    int_b = min(int_b, int2_b), 
    int_r = min(int_r, int2_r);
    
    R = R.submatrix(int_t, int_l, int_b - int_t, int_r - int_l).deep_copy();
    G = G.submatrix(int_t - RGh_shift, int_l - RGw_shift, int_b - int_t, int_r - int_l).deep_copy();
    B = B.submatrix(int_t - RBh_shift, int_l - RBw_shift, int_b - int_t, int_r - int_l).deep_copy();
    h = R.n_rows;
    w = R.n_cols;

    // return 0;
} 

double get_metric(Image &img1, Image &img2, int h_shift, int w_shift, int metric){
    int int_t = max(0, h_shift),
        int_l = max(0, w_shift), 
        int_b = min(img1.n_rows, img2.n_rows + h_shift), 
        int_r = min(img1.n_cols, img2.n_cols + w_shift);
    // cout << h_shift << ' ' << w_shift << endl;
    // cout << int_t << ' ' << int_l << ' ' << int_b << ' ' << int_r << ' ' << endl; 
    // cout << img1.n_rows << ' ' << img1.n_cols << endl; 
    // cout << img2.n_rows << ' ' << img2.n_cols << endl; 
    Image t1 = img1.submatrix(int_t, int_l, int_b - int_t, int_r - int_l);
    Image t2 = img2.submatrix(int_t - h_shift, int_l - w_shift, int_b - int_t, int_r - int_l);
    double result = 0;
    uint h = int_b - int_t, w = int_r - int_l;
    if(metric == M_MSE){
        for(uint i = 0; i < h; i++)
            for(uint j = 0; j < w; j++)
                result += pow(max(0, min(get<0>(t1(i, j)), 255)) - max(0, min(get<0>(t2(i, j)), 255)), 2);
        result /= double(h*w);
    }
    else // cross-correlation
        for(uint i = 0; i < h; i++)
            for(uint j = 0; j < w; j++)
                result += max(0, min(get<0>(t1(i, j)), 255)) * max(0, min(get<0>(t2(i, j)), 255));
    return result;
}