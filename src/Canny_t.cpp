#include "Canny.h"

using std::tuple;
using std::get;
using std::make_tuple;

Canny::Canny(const Image &src, uint th1, uint th2): source(src), result(src.n_rows, src.n_cols),
												h(src.n_rows), w(src.n_cols), t1(th1), t2(th2),
												G(h, w), visited(h, w){
	source = gaussian_separable(src, 1.4, 2);
	Image Ix = sobel_x(source), Iy = sobel_y(source);
	Matrix<double> Gd(h, w); // gradient direstions
	// cout << "beginning canny" << endl;
	for(uint x = 0; x < h; x++) // calculating gradients
		for(uint y = 0; y < w; y++){
			G(x, y) = sqrt(pow( int(get<0>( Ix(x, y) )), 2) + pow( int(get<0>( Iy(x, y) )), 2));
			Gd(x, y) = atan2(int(get<0>( Ix(x, y) )), int(get<0>( Ix(x, y) )));
			// cout << G(x, y) << ':' << get<0>( Ix(x, y) ) << ':' << get<0>( Iy(x, y) ) << endl;
		}
	// cout << "beginning 1" << endl;
	Matrix<double> Gsup = G.deep_copy();
	for(uint x = 1; x < h - 1; x++) // suppressing non-max
		for(uint y = 1; y < w - 1; y++){
			double Gdeg = Gd(x, y);
			double Grad = G(x, y), N0rad = 0, N1rad = 0;
			

			int id = int((Gdeg - M_PI/8 + 2*M_PI) / (M_PI/4)) % 4;
			switch(id) { // fixme
				case 2: 
					N0rad = get_double(&G, x+1, y-1);
					N1rad = get_double(&G, x-1, y+1);
					break; 
				case 3:
					N0rad = get_double(&G, x, y+1);
					N1rad = get_double(&G, x, y-1);
					break; 
				case 0:
					N0rad = get_double(&G, x+1, y+1);
					N1rad = get_double(&G, x-1, y-1);
					break; 
				case 1: 
					N0rad = get_double(&G, x+1, y);
					N1rad = get_double(&G, x-1, y);
					break;
				default: ; 
			}
			if(!(Grad > N0rad && Grad > N1rad))
				Gsup(x, y) = 0;
		}
	Matrix<double> G1 = G.deep_copy();
	G = Gsup.deep_copy();
	for(uint i = 0; i < h; i++) 
		for(uint j = 0; j < w; j++){
			visited(i, j) = false;
		}
	for(uint x = 0; x < h; x++)  // two thresholds
		for(uint y = 0; y < w; y++){
			if(G(x, y) <= t1){
				result(x, y) = make_tuple(0, 0, 0);
				// visited(x, y) = true;
			} else if(G(x, y) > t2){
				result(x, y) = make_tuple(-1, -1, -1); // white
				// visited(x, y) = true;
		}
	}
	for(uint x = 0; x < h; x++) // there goes edge detection itself
		for(uint y = 0; y < w; y++)
			if(!visited(x, y))
				is_edge(x, y);

	// part 2

	// Gsup = G1.deep_copy();
	// for(uint x = 1; x < h - 1; x++) // suppressing non-max
	// 	for(uint y = 1; y < w - 1; y++){
	// 		double Gdeg = Gd(x, y);
	// 		double Grad = G1(x, y), N0rad = 0, N1rad = 0;
			

	// 		int id = int((Gdeg - M_PI/8 + 2*M_PI) / (M_PI/4)) % 4;
	// 		switch(id) { // fixme
	// 			case 0: 
	// 				N0rad = get_double(&G1, x+1, y-1);
	// 				N1rad = get_double(&G1, x-1, y+1);
	// 				break; 
	// 			case 1:
	// 				N0rad = get_double(&G1, x, y+1);
	// 				N1rad = get_double(&G1, x, y-1);
	// 				break; 
	// 			case 2:
	// 				N0rad = get_double(&G1, x+1, y+1);
	// 				N1rad = get_double(&G1, x-1, y-1);
	// 				break; 
	// 			case 3: 
	// 				N0rad = get_double(&G1, x+1, y);
	// 				N1rad = get_double(&G1, x-1, y);
	// 				break;
	// 			default: ; 
	// 		}
	// 		if(!(Grad > N0rad && Grad > N1rad))
	// 			Gsup(x, y) = 0;
	// 	}
	// G1 = G.deep_copy();
	// G = Gsup.deep_copy();
	// visited = Matrix<bool>(h, w).deep_copy();
	// Image result1 = result.deep_copy();
	// result = Image(h, w).deep_copy(); // ?
	// for(uint i = 0; i < h; i++) 
	// 	for(uint j = 0; j < w; j++){
	// 		visited(i, j) = false;
	// 	}
	// for(uint x = 0; x < h; x++)  // two thresholds
	// 	for(uint y = 0; y < w; y++){
	// 		if(G(x, y) <= t1){
	// 			result(x, y) = make_tuple(0, 0, 0);
	// 			// visited(x, y) = true;
	// 		} else if(G(x, y) > t2){
	// 			result(x, y) = make_tuple(-1, -1, -1); // white
	// 			// visited(x, y) = true;
	// 	}
	// }
	// for(uint x = 0; x < h; x++) // there goes edge detection itself
	// 	for(uint y = 0; y < w; y++)
	// 		if(!visited(x, y))
	// 			is_edge(x, y);

	// for(uint x = 0; x < h; x++) // combine 'em
	// 	for(uint y = 0; y < w; y++){
	// 		if(result(x, y) == make_tuple(0, 0, 0) && result1(x, y) == make_tuple(0, 0, 0))
	// 			result(x, y) = make_tuple(0, 0, 0);
	// 		else 
	// 			result(x, y) == make_tuple(-1, -1, -1);
	// 	}
}

bool Canny::is_edge(uint x, uint y){
	if(x >= h || y >= w) // our of borders
		return false;

	if(visited(x, y)){
		if(result(x, y) == make_tuple(0, 0, 0)){
			return false;
		} else if(result(x, y) == make_tuple(-1, -1, -1))
			return true;
	} else 
		visited(x, y) = true;

	if(result(x, y) == make_tuple(0, 0, 0)){
		return false;
	} else if(result(x, y) == make_tuple(-1, -1, -1)){
		return true;
	} else if(is_edge(x+1, y) || is_edge(x, y+1) || is_edge(x-1, y) || is_edge(x, y-1) ||
			  is_edge(x+1, y+1) || is_edge(x+1, y-1) || is_edge(x-1, y+1) || is_edge(x-1, y-1)){
		result(x, y) = make_tuple(-1, -1, -1);
		return true;
	} else {
		result(x, y) = make_tuple(0, 0, 0);
		return false;
	}

	return false;    
}